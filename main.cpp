#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>
#include <semaphore.h>


// Declare our Semaphores
sem_t agent, tobacco, paper, match;
sem_t tobacco_sem, paper_sem, match_sem, mutex;

// Declare our bool flags
bool is_tobacco = false;
bool is_paper = false;
bool is_match = false;

// Define our agents
void* tp_agent(void* vargp) {
    for (int i = 0; i < 6; ++i) {
        int wait = (rand() % 200) / 1000;
        sleep(wait);

        sem_wait(&agent);
        sem_post(&tobacco);
        sem_post(&paper);
    }
}

void* pm_agent(void* vargp) {
    for (int i = 0; i < 6; ++i) {
        int wait = (rand() % 200) / 1000;
        sleep(wait);

        sem_wait(&agent);
        sem_post(&paper);
        sem_post(&match);
    }
}

void* tm_agent(void* vargp) {
    for (int i = 0; i < 6; ++i) {
        int wait = (rand() % 200) / 1000;
        sleep(wait);

        sem_wait(&agent);
        sem_post(&tobacco);
        sem_post(&match);
    }
}


// Define our pushers
void* tobacco_pusher(void* vargp) {
    for (int i = 0; i < 12; ++i) {
        sem_wait(&tobacco);
        sem_wait(&mutex);
        
        if (is_paper) {
            is_paper = false;
            sem_post(&match_sem);
        }
        else if (is_match) {
            is_match = false;
            sem_post(&paper_sem);
        }
        else {
            is_tobacco = true;
        }

        sem_post(&mutex);
    }
}

void* match_pusher(void* vargp) {
    for (int i = 0; i < 12; ++i) {
        sem_wait(&match);
        sem_wait(&mutex);

        if (is_paper) {
            is_paper = false;
            sem_post(&tobacco_sem);
        }
        else if (is_tobacco) {
            is_tobacco = false;
            sem_post(&paper_sem);
        }
        else {
            is_match = true;
        }

        sem_post(&mutex);
    }
}

void* paper_pusher(void* vargp) {
    for (int i = 0; i < 12; ++i) {
        sem_wait(&paper);
        sem_wait(&mutex);

        if (is_match) {
            is_match = false;
            sem_post(&tobacco_sem);
        }
        else if (is_tobacco) {
            is_tobacco = false;
            sem_post(&match_sem);
        }
        else {
            is_paper = true;
        }

        sem_post(&mutex);
    }
}

// Define our smokers
void* tobacco_smoker(void* vargp) {
    for (int i = 0; i < 3; ++i) {
        sem_wait(&tobacco_sem);

        int wait = (rand() % 50) / 1000;
        sleep(wait);

        is_match = false;
        is_tobacco = false;
        is_paper = false;

        sem_post(&agent);
    }
    printf("Smoker with tobacco said something about being hungry...\n");
}

void* paper_smoker(void* vargp) {
    for (int i = 0; i < 3; ++i) {
        sem_wait(&paper_sem);

        int wait = (rand() % 50) / 1000;
        sleep(wait);

        is_match = false;
        is_tobacco = false;
        is_paper = false;

        sem_post(&agent);
    }
    printf("Smoker with paper said something about being hungry...\n");
}

void* match_smoker(void* vargp) {
    for (int i = 0; i < 3; ++i) {
        sem_wait(&match_sem);

        int wait = (rand() % 50) / 1000;
        sleep(wait);

        is_match = false;
        is_tobacco = false;
        is_paper = false;

        sem_post(&agent);
    }
    printf("Smoker with matches said something about being hungry...\n");
}

int main() {
    // Seed for the random number
    srand(time(NULL));

    // Create our 12 threads
    pthread_t ag1, ag2, ag3;
    pthread_t push1, push2, push3;
    pthread_t smoker1, smoker2, smoker3, smoker4, smoker5, smoker6;

    // Initialize our semaphores
    sem_init(&agent, 0, 1);
    sem_init(&tobacco, 0, 0);
    sem_init(&paper, 0, 0);
    sem_init(&match, 0, 0);
    sem_init(&tobacco_sem, 0, 0);
    sem_init(&paper_sem, 0, 0);
    sem_init(&match_sem, 0, 0);
    sem_init(&mutex, 0, 1);

    // Start up the threads
    pthread_create(&ag1, NULL, pm_agent, NULL);
    pthread_create(&ag2, NULL, tp_agent, NULL);
    pthread_create(&ag3, NULL, tm_agent, NULL);
    pthread_create(&push1, NULL, tobacco_pusher, NULL);
    pthread_create(&push2, NULL, paper_pusher, NULL);
    pthread_create(&push3, NULL, match_pusher, NULL);
    pthread_create(&smoker1, NULL, tobacco_smoker, NULL);    
    pthread_create(&smoker2, NULL, tobacco_smoker, NULL);
    pthread_create(&smoker3, NULL, paper_smoker, NULL);
    pthread_create(&smoker4, NULL, paper_smoker, NULL);
    pthread_create(&smoker5, NULL, match_smoker, NULL);
    pthread_create(&smoker6, NULL, match_smoker, NULL);     

    // Join the threads when they complete execution
    pthread_join(ag1, NULL);
    pthread_join(ag2, NULL);
    pthread_join(ag3, NULL);
    pthread_join(push1, NULL);
    pthread_join(push2, NULL);
    pthread_join(push3, NULL);
    pthread_join(smoker1, NULL);
    pthread_join(smoker2, NULL);
    pthread_join(smoker3, NULL);
    pthread_join(smoker4, NULL);
    pthread_join(smoker5, NULL);
    pthread_join(smoker6, NULL);

    printf("Program terminating.\n");

    return 0;
}
